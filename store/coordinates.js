export const state = () => ({
  coordinates: {
    startPoint: ' ',
    endPoint: ' ',
  },
})

export const mutations = {
  setCoordinates: (state, { startPoint, endPoint }) => {
    console.log('store:' + startPoint, endPoint)
    state.coordinates.startPoint = startPoint
    state.coordinates.endPoint = endPoint
  },
}

export const getters = {
  getCoordinates: (state) => {
    return state.coordinates
  },
}
