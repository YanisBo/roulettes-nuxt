export const state = () => ({
  isEditMode: false,
})

export const mutations = {
  toggleMode: (state) => (state.isEditMode = !state.isEditMode),
}

export const getters = {
  getMode: (state) => {
    return state.isEditMode
  },
}
