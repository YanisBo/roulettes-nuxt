export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'roulettes',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [{ src: '~/plugins/persistedState.client.js' }],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    '@nuxtjs/axios',
    'nuxt-leaflet',
    '@nuxtjs/dotenv',
    '@nuxtjs/apollo',
  ],

  apollo: {
    clientConfigs: {
      default: '~/gql/apollo-config.js',
    },
  },
  // Modules (https://go.nuxtjs.dev/config-modules)

  modules: [
    // https://go.nuxtjs.dev/bootstrap
    '@nuxtjs/bulma',
    'nuxt-i18n',
  ],
  i18n: {
    locales: [
      {
        code: 'en',
        iso: 'en-US',
        name: 'English',
        file: 'en.json',
      },
      {
        code: 'fr',
        iso: 'fr-FR',
        name: 'Français',
        file: 'fr.json',
      },
    ],
    defaultLocale: 'en',
    strategy: 'prefix', //always append the language prefix in the URL
    lazy: true, //only loads preferred language
    langDir: 'locales/',
    vueI18n: {
      fallbackLocale: ['en', 'fr'], // choose which language to use when your preferred language lacks a translation
      messages: {
        en: require('./locales/en.json'),
        fr: require('./locales/fr.json'),
      },
    },
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    postcss: {
      preset: {
        features: {
          customProperties: false,
        },
      },
    },
  },
}
